<?php
/**
 * Created by PhpStorm.
 * User: sheldon
 * Date: 18-1-22
 * Time: 下午1:55
 */

include_once 'config.php';

if (isset($_GET['type']) && in_array($_GET['type'], [0, 1, 2, 3, 4, 5, 6])) {
    $type = $_GET['type'];

    include_once "template/lottery.php";
    exit;
}


include_once 'template/index.php';