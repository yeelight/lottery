<?php
/**
 * Created by PhpStorm.
 * User: sheldon
 * Date: 18-1-22
 * Time: 下午1:55
 */

include_once 'config.php';

$action = isset($_GET['action']) ? $_GET['action'] : 'list';

switch ($action) {
    case "delete":
        $key = isset($_POST['key']) ? $_POST['key'] : '0';
        foreach ($users as $k => $user) {
            if ($key == $k) {
                unset($users[$k]);
            }
        }
        shuffle($users);
        file_put_contents("data/users.json", json_encode($users));
        $success = "删除成功";
        exit($success);
        break;
    case "add":
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        if ($name && userNotExist($users, $name)) {
            array_push($users, $name);
        } else {
            exit("用户已存在");
        }

        shuffle($users);
        file_put_contents("data/users.json", json_encode($users));
        $success = "添加成功";
        exit($success);
        break;
    case "lists":
        shuffle($users);
        exit(json_encode([
            "code" => 0,
            "message" => $users
        ]));
        break;
    case "lottery":
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $award_id = isset($_POST['award_id']) ? $_POST['award_id'] : 0;
        $awards = getPrizeAwardsById($prizes, $type);
        $award = $awards[$award_id];
        $numbers = $award['count'];

        if ($type == 0) {
            $users = $article_users;
        }

        if (empty($users)) {
            exit(json_encode([
                "code" => 0,
                "message" => '用户都被抽完了'
            ]));
        }
        if (isset($type) && !in_array($type, [0, 1, 2, 3, 4, 5, 6])) {
            exit(json_encode([
                "code" => 0,
                "message" => '非法'
            ]));
        }

        if (in_array($type, [0, 1, 2, 3])) {
            $users = array_diff($users, $ignores);
        }

        //摇色子
        shuffle($users);
        $max = count($users);
        if ($max < $numbers) {
            exit(json_encode([
                "code" => 0,
                "message" => "奖池内人数太少"
            ]));
        }

        if ($numbers == 0) {
            exit(json_encode([
                "code" => 0,
                "message" => "该奖品已经被抽完了，请换到下一个奖品！"
            ]));
        }

        $current_winners = [];
        for ($i = 0; $i < $numbers; $i++) {
            //读取接口随机数
            $winnerNum = my_random($max - $i);
            //有可能网络错误，请求不到接口数据

            if (!$winnerNum) {
                exit(json_encode([
                    "code" => 0,
                    "message" => "没抽到"
                ]));
            }
            //取得胜出者
            $current_winners[] = $users[intval($winnerNum)-1];

            //用户池中剔除中奖者
            unset($users[intval($winnerNum)-1]);

            //剩余用户再摇一遍色子，并写入用户池
            shuffle($users);
        }

        $prizes = setAwardCountByIdAndAwardId($prizes, $type, $award_id);
        file_put_contents("data/prizes.json", json_encode($prizes));
        if ($type == 0) {
            file_put_contents("data/article_users.json", json_encode($users));
        } else {
            file_put_contents("data/users.json", json_encode($users));
        }

        //胜出者写入获奖池
        foreach ($current_winners as $index => $current_winner) {
            array_push($winners, [
                "name" => $current_winner,
                "prize_id" => $type,
                "prize_type" => getPrizeNameById($prizes, $type),
                "award" => getPrizeAwardByIdAndAwardId($prizes, $type, $award_id)
            ]);
        }

        file_put_contents("data/winners.json", json_encode($winners));
        exit(json_encode([
            "code" => 0,
            "message" => implode(" ", $current_winners)
        ]));
        break;
}

include_once 'template/manage.php';