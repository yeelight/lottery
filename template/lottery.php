<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>抽奖管理</title>

    <style>
        html {
            background: url(../images/<?php echo $type;?>.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .lottery-prize {
            position: fixed;
            bottom: 42%;
            font-size: 70px;
            line-height: 120px;
            left:30%;
            width: 40%;
            color: #fff;
            text-align: center;
        }
        .winner-users {
            position: fixed;
            bottom: 10%;
            width: 60%;
            left: 20%;
            line-height: 60px;
            font-size: 42px;
            color: red;
            text-align: center;
            padding: 20px;
            border: 5px solid red;
        }
    </style>
</head>
<body>

    <div class="lottery-prize">

    </div>
    <div class="winner-users">
        等待中奖区
    </div>
    <audio id="myaudio" src="../images/lotoery.wav" controls="controls" loop="false" hidden="true" ></audio>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../bootstrap/jquery.min.js"></script>

        <script>
            var t;
            var lotteryUser = '';
            var running = false;
            var myAuto = document.getElementById('myaudio');
            var users = <?php echo json_encode($users);?>;
            var awards = <?php echo json_encode(getPrizeAwardsById($prizes, $type));?>;
            var current_award_id = 0;
            initAward();

            function initAward() {
                $(".lottery-prize").html(awards[current_award_id]['name']);
            }
            function timedCount() {
                var winner_users = '';
                var max = users.length;
                var count = awards[current_award_id]['count'];
                for (var i = 0; i < count; i++) {
                    var random = parseInt(Math.random()*((max - i)+1),10);
                    winner_users += users[random];
                }
                if (count == 0) {
                    winner_users = '该奖品已经被抽完了，请换到下一个奖品！';
                    stopCount();
                    $(".winner-users").html(winner_users);
                    myAuto.pause();
                    running = false;
                } else {
                    $(".winner-users").html(winner_users);

                    t = setTimeout("timedCount()", 10)
                }
            }

            function stopCount() {
                clearTimeout(t)
            }
            function lottery() {
                var type = <?php echo $type;?>;
                running = true;
                $.ajax({
                    url:'/user.php?action=lottery',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    data:{
                        type:type,
                        award_id: current_award_id
                    },
                    timeout:5000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    beforeSend: function(){
                        timedCount();
                    },
                    success:function(data){
                        lotteryUser   = data.message;
                    }
                });
            }

            $("body").keydown(function (e) {
                if(!e) var e = window.event;
                // 按空格开始抽奖滚动
                if(e.keyCode==32){
                    if (running == false) {
                        myAuto.play();
                        getUsers();
                    }
                }
                // 按Esc停止抽奖滚动
                if(e.keyCode==27){
                    stopCount();
                    $(".winner-users").html(lotteryUser);
                    myAuto.pause();
                    running = false;
                }
                // 按回车更换奖项
                if(e.keyCode==13 ){
                    if (awards.length == 1) {
                        alert("该等级奖品只有一种");
                    }
                    if (awards.length <= (current_award_id + 1)) {
                        current_award_id = 0;
                    } else {
                        current_award_id += 1;
                    }

                    initAward();
                }
            });

            function getUsers() {
                $.ajax({
                    url:'/user.php?action=lists',
                    type:'GET', //GET
                    async:true,    //或false,是否异步
                    timeout:5000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text

                    success:function(data){
                        users = data.message;
                        lottery();
                    }
                });
            }
        </script>
</body>
</html>