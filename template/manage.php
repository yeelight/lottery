<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>抽奖管理</title>

    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bootstrap/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="manage.php">抽奖管理</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="manage.php">用户管理</a></li>
                    <li><a href="index.php">抽奖</a></li>
                    <li><a href="winners.php">获奖者</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>

    <div class="container">

        <div class="starter-template table-responsive">
            <?php if (!empty($success)) { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $success;?>
                </div>
            <?php }?>
            <?php if (!empty($error)) { ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $error;?>
                </div>
            <?php }?>
            <?php if (!empty($warning)) { ?>
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo $warning;?>
                </div>
            <?php }?>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#addModal">
                添加用户
            </button>
            <br >
            <br >
            <table class="table table-bordered">
                <?php if (!empty($users)) { ?>
                    <div class="row">
                    <?php foreach ($users as $key => $user) { ?>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-default lottery-user" data-id="<?php echo $key;?>">
                                <?php echo $user;?>
                                </button>
                            </div>
                        <?php if ($key % 3 == 2) { ?>
                            </div>
                            <br >
                            <div class="row">
                        <?php } ?>
                    <?php } ?>
                    </div>
                <?php } ?>
            </table>
        </div>

    </div><!-- /.container -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">添加用户</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <span class="input-group-addon" >姓名</span>
                        <input type="text" class="form-control" placeholder="姓名" id="add_name" name="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-primary" id="add_user">添加</button>
                </div>
            </div>
        </div>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../bootstrap/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../bootstrap/main.js"></script>
    <script>
        $(".lottery-user").dblclick(function () {
            var key = $(this).attr("data-id");
            if (confirm("确定删除该用户？")) {
                $.ajax({
                    url:'/user.php?action=delete',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    data:{
                        key:key
                    },
                    timeout:5000,    //超时时间
                    dataType:'text',    //返回的数据格式：json/xml/html/script/jsonp/text

                    success:function(data){
                        alert(data);
                        window.location.reload();
                    }
                });
            }
        });
        $("#add_user").click(function () {
            var name = $("#add_name").val();
            $.ajax({
                url:'/user.php?action=add',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    name:name
                },
                timeout:5000,    //超时时间
                dataType:'text',    //返回的数据格式：json/xml/html/script/jsonp/text

                success:function(data){
                    alert(data);
                    window.location.reload();
                }
            });
        });
    </script>
</body>
</html>